import React, { Component } from 'react';
import {
  Text,
  View
} from 'react-native';

// https://github.com/skv-headless/react-native-scrollable-tab-view

export default class App extends Component {
  constructor(){
    super();
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>Test</Text>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
}
